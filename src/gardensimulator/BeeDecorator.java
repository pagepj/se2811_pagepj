package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Decorator that allows additional functionality to be added to bees at runtime.
 */
public abstract class BeeDecorator implements Bee {

    protected Bee wrappedBee;
    protected boolean active;
    protected Image decoratorSprite;
    private static final int SIZE = 50;

    /**
     * Constructor that passes in a bee to be wrapped
     * @param bee bee to be wrapped
     */
    public BeeDecorator(Bee bee) {
        this.wrappedBee = bee;
        this.active = true;
    }

    /**
     * Default constructor allows creating empty decorators as templates
     */
    public BeeDecorator(){}

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param garden the garden the bee lives in
     * @return coordinates moved to
     */
    @Override
    public Point2D move(Garden garden) {
        return wrappedBee.move(garden);
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @return current coordinates
     */
    @Override
    public Point2D getLocation() {
        return wrappedBee.getLocation();
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param newLocation new location
     */
    @Override
    public void setLocation(Point2D newLocation) {
        wrappedBee.setLocation(newLocation);
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param amount amount to add
     */
    @Override
    public void addEnergy(int amount) {
        wrappedBee.addEnergy(amount);
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @return whether or not bee is dead
     */
    @Override
    public boolean isDead() {
        return wrappedBee.isDead();
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @return bee's type (which subclass of ConcreteBee)
     */
    @Override
    public String getType() {
        return wrappedBee.getType();
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param ticks ticks to immobilize bee
     */
    @Override
    public void setMovementTimeout(int ticks) {
        wrappedBee.setMovementTimeout(ticks);
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param ctx context to give access to the canvas
     */
    @Override
    public void draw(GraphicsContext ctx) {
        wrappedBee.draw(ctx);
        if (active) {
            ctx.drawImage(decoratorSprite, getLocation().getX(), getLocation().getY(), SIZE, SIZE);
        }
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @return speed
     */
    @Override
    public int getSpeed() {
        return wrappedBee.getSpeed();
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param speed factor to multiply movements by
     */
    @Override
    public void setSpeed(int speed) {
        wrappedBee.setSpeed(speed);
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param img image to set
     */
    @Override
    public void setSprite(Image img) {
        wrappedBee.setSprite(img);
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @return energy
     */
    @Override
    public int getEnergy() {
        return wrappedBee.getEnergy();
    }

    /**
     * delegate functionality to wrapped bee unless this method is overridden
     * @param target coordinates to move toward
     */
    @Override
    public void moveToward(Point2D target) {
        wrappedBee.moveToward(target);
    }
}
