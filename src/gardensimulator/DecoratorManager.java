package gardensimulator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Decorator used to add new decorators to bees without having to change the references to them
 */
public class DecoratorManager extends BeeDecorator {

    public DecoratorManager(Bee bee) {
        super(bee);
    }

    /**
     * Applies a decorator to the wrapped bee, keeping this decorator as the outside wrapper
     * @param decorator decorator to apply
     */
    public void apply(BeeDecorator decorator) {

        try {
            Class<?> decoratorClass = Class.forName(decorator.getClass().getName());
            Constructor<?> constructor = decoratorClass.getConstructor(Bee.class);
            this.wrappedBee = (BeeDecorator)constructor.newInstance(new Object[] { this.wrappedBee });
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            System.err.println("Error applying decorator to bee.");
        }

    }
}
