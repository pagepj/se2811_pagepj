package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;


/**
 * This class represents an abstract bee. Concrete bee classes inherit from this and implement move() with
 * various movement patterns.
 */
public abstract class ConcreteBee implements Bee {

    private static final int STARTING_ENERGY = 300;
    private static final int DEFAULT_SPEED = 2;
    private static final int DRAW_SIZE = 50;

    private Point2D location;
    private int energy;
    private boolean dead;
    private int movementTimeout = 0;
    private int speed;
    private Image sprite;


    /**
     * Default constructor
     * @param location initial location of the bee
     */
    public ConcreteBee(Point2D location) {
        this.energy = STARTING_ENERGY;
        this.location = location;
        this.speed = DEFAULT_SPEED;
        this.sprite = new Image("file:res/bee.png");
    }

    /**
     * Move the bee the distance travelled in one frame. The bias is an external point that may or may not factor in
     * to the bee's movement strategy (i.e. moving toward a flower)
     * @return new location
     */
    public abstract Point2D move(Garden garden);

    /**
     * Check to see if the bee is on the edge of the screen. If so, move the bee to avoid going off the edge.
     * @param garden the garden where the bee lives.
     */
    public void checkEdges(Garden garden) {
        Point2D l = this.getLocation();
        double x = l.getX();
        double y = l.getY();
        double w = garden.getWidth() - garden.getSpriteSize();
        double h = garden.getHeight() - garden.getSpriteSize();

        if (x > w) {
            this.setLocation(new Point2D(x - 5, y));
        } else if (x < 0) {
            this.setLocation(new Point2D(5, y));
        } else if (y > h) {
            this.setLocation(new Point2D(x, y - 5));
        } else if (y < 0) {
            this.setLocation(new Point2D(x, 5));
        }
    }

    /**
     * Get the speed of the bee
     * @return speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Set the speed of the bee.
     * @param speed the new speed
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * Get bee's current location
     * @return location
     */
    public Point2D getLocation() {
        return location;
    }

    /**
     * Set the location of the bee
     * @param location the new location
     */
    public void setLocation(Point2D location) {
        if (movementTimeout <= 0) {
            this.location = location;
        } else {
            movementTimeout--;
        }
    }

    /**
     * Move toward a specific location.
     * @param target coordinates to move toward
     */
    public void moveToward(Point2D target) {

        double distance_x = getLocation().getX() - target.getX();
        double distance_y = getLocation().getY() - target.getY();

        double angle = Math.tan(distance_y / distance_x);

        // add some uncertainty to the angle, but still move toward the target overall
        angle -= 5;
        angle += Math.random();

        double dx = getSpeed() * Math.cos(angle);
        double dy = getSpeed() * Math.sin(angle);

        setLocation(getLocation().add(dx, dy));
        getLocation().add(dx, dy);
    }

    /**
     * Add the given amount of energy
     * @param amount the amount to add
     */
    public void addEnergy(int amount) {
        energy += amount;
    }

    /**
     * Get bee's current energy level
     * @return energy level
     */
    public int getEnergy() {
        return energy;
    }

    /**
     * Whether or not the bee is dead (has energy <= 0)
     * @return whether or not the bee is dead
     */
    public boolean isDead() {
        dead = energy <= 0;
        return dead;
    }

    /**
     * Get the type of bee
     * @return bee type
     */
    public abstract String getType();

    /**
     * Stop the bee from moving for a certain amount of ticks
     * @param ticks ticks to stop
     */
    public void setMovementTimeout(int ticks) {
        movementTimeout = ticks;
    }

    /**
     * Set the sprite of the bee to an image
     * @param img image to set
     */
    public void setSprite(Image img) {
        this.sprite = img;
    }

    /**
     * Draw the bee
     * @param ctx context used to draw the bee to the canvas
     */
    public void draw(GraphicsContext ctx) {
        ctx.drawImage(this.sprite,
                this.getLocation().getX(), this.getLocation().getY(),
                DRAW_SIZE, DRAW_SIZE);
        ctx.setFill(Paint.valueOf("White"));
        ctx.fillText(String.valueOf(this.getEnergy()), this.getLocation().getX(), this.getLocation().getY());
    }
}
