package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * Bee decorator that multiplies the bee's speed by 2
 */
public class SpeedBoost extends BeeDecorator {

    private int timeToLive;

    private static final int SCALE_FACTOR = 2;
    private static final int TIME_TO_LIVE = 100;

    /**
     * wrap the bee with the new functionality
     * @param bee bee to wrap
     */
    public SpeedBoost(Bee bee) {
        super(bee);
        wrappedBee.setSpeed(bee.getSpeed() * SCALE_FACTOR);
        this.decoratorSprite = new Image("file:res/speedBoost.png");
        timeToLive = TIME_TO_LIVE; //ticks
    }

    /**
     * Default constructor to allow templates
     */
    public SpeedBoost() {}

    /**
     * Use the move method to decrement timeToLive
     * @param garden the garden the bee lives in
     * @return new coordinates
     */
    @Override
    public Point2D move(Garden garden) {
        if (timeToLive < 0) {
            active = false;
        } else {
            timeToLive--;
        }

        return wrappedBee.move(garden);
    }
}
