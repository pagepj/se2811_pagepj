package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * Bee decorator that causes the bee to move toward a draining flower.
 */
public class Hypnosis extends BeeDecorator {

    /**
     * Wrap the bee with the new functionality
     * @param bee bee to wrap
     */
    public Hypnosis(Bee bee) {
        super(bee);
        this.decoratorSprite = new Image("file:res/hypnosis.png");
    }

    /**
     * Default constructor to allow templates
     */
    public Hypnosis(){}

    private int lifetime = 50; // minimum time for effect to remain (in ticks)

    /**
     * Move the bee toward a draining flower
     * @param garden the garden the bee lives in
     * @return new bee coordinates
     */
    @Override
    public Point2D move(Garden garden) {

        if (lifetime > 0) {
            lifetime--;
        }

        if (active) {
            Point2D target = new Point2D(0, 0);

            for (Flower flower : garden.getFlowers()) {
                if (flower instanceof FlowerDrainEnergy) {
                    target = flower.getLocation();
                }
            }

            for (Flower flower : garden.getFlowers()) {
                if (flower instanceof FlowerProvideEnergy &&
                        flower.getLocation().distance(flower.getLocation()) < 20 &&
                        lifetime <= 0) {

                    active = false;
                }
            }

                moveToward(target);
            return getLocation();

        } else {
            return wrappedBee.move(garden);
        }
    }
}
