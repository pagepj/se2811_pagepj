package gardensimulator;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;

/**
 * Controller for the javafx application
 */
public class Controller {

    @FXML
    public ImageView imageView;
    @FXML
    public Label stepLabel;
    @FXML
    private Canvas canvas;

    private Garden garden;
    private AnimationTimer animationTimer;

    /**
     * Runs when the program is started
     */
    public void initialize(){

        Image key = new Image("file:res/key.png");
        imageView.setImage(key);

        int SPRITE_SIZE = 50;
        garden = new Garden(canvas.getWidth(), canvas.getHeight(), SPRITE_SIZE, 9, 12);

        animationTimer = new AnimationTimer() {
            public void handle(long currentNanoTime) {
                step();
            }
        };
        animationTimer.start();
    }

    /**
     * Increments the program
     */
    public void step() {
        garden.step();

        if (garden.isDone()) {
            animationTimer.stop();
            Alert endGame = new Alert(Alert.AlertType.INFORMATION,
                    "It took " + garden.getStepCount() +
                            " steps for all the bees to " + "die.", ButtonType.OK);
            endGame.setOnHidden(e -> Platform.exit());
            endGame.show();
        }
        stepLabel.setText(String.format("Step count: %d", garden.getStepCount()));

        garden.draw(canvas.getGraphicsContext2D());
    }

    /**
     * Starts the animation
     */
    public void handleStart() {
        animationTimer.start();
    }

    /**
     * Stops the animation
     */
    public void handleStop() {
        animationTimer.stop();
    }

    /**
     * Makes the program step when up or right key is pressed.
     * @param keyEvent information about the key pressed
     */
    public void handleKey(KeyEvent keyEvent) {
        switch (keyEvent.getCode()) {
            case UP:
            case RIGHT:
                step();
                break;
            default:
                break;
        }
    }
}
