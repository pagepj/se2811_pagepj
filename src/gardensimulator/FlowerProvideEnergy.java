package gardensimulator;

import javafx.geometry.Point2D;

/**
 * Specific implementation of Flower. Drains energy from bees that visit.
 */
public class FlowerProvideEnergy extends Flower {

    /**
     * Default constructor.
     *
     * @param location initial location of the flower
     */
    public FlowerProvideEnergy(Point2D location, BeeDecorator decorator) {
        super(location, decorator);
    }

    /**
     * Give one nectar point to the bee
     * @param beeEnergy current energy level of bee
     * @return amount to give
     */
    public int transferEnergy(int beeEnergy) {
        super.changeNectarEnergy(-1);
        return 1;
    }
}

