package gardensimulator;


import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * Specific implementation of Flower. Drains energy from bees that visit.
 */
public class FlowerDrainEnergy extends Flower{

    /**
     * Default constructor.
     * @param location initial location of the flower
     */
    public FlowerDrainEnergy(Point2D location, BeeDecorator decorator){
        super(location, decorator);
        setSprite(new Image("file:res/flower2.png"));
    }

    /**
     * Takes one energy point from the bee
     * @param beeEnergy current energy level of the bee
     * @return energy to drain from the bee
     */
    public int transferEnergy(int beeEnergy){
        super.changeNectarEnergy(1);
        return -1;
    }
}
