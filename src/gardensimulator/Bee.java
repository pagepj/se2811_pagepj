package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Interface used to interact with bees. Allows decorators to be used seamlessly.
 */
public interface Bee {

    /**
     * Move the bee according to its strategy
     * @param garden the garden the bee lives in
     * @return the new position of the Bee
     */
    Point2D move(Garden garden);

    /**
     * Get the location of the bee
     * @return coordinates
     */
    Point2D getLocation();

    /**
     * Set a precise location for the bee
     * @param newLocation new location
     */
    void setLocation(Point2D newLocation);

    /**
     * Add energy to the bee (pass in negative number to subtract)
     * @param amount amount to add
     */
    void addEnergy(int amount);

    /**
     * Whether or not the bee's energy level is 0
     * @return is the bee dead?
     */
    boolean isDead();

    /**
     * Get the type of bee. Defined by subclasses.
     * @return the type
     */
    String getType();

    /**
     * Set a time during which bees will not be able to move
     * @param ticks ticks to immobilize bee
     */
    void setMovementTimeout(int ticks);

    /**
     * Set the bee's speed factor
     * @param speed factor to multiply movements by
     */
    void setSpeed(int speed);

    /**
     * Get the bee's speed factor
     * @return factor movement is currently being multiplied by
     */
    int getSpeed();

    /**
     * Draw the bee to the canvas
     * @param ctx context to give access to the canvas
     */
    void draw(GraphicsContext ctx);

    /**
     * Set the sprite of the bee to an image
     * @param img image to set
     */
    void setSprite(Image img);

    /**
     * Get the energy point of the bee
     * @return energy points
     */
    int getEnergy();

    /**
     * Move toward a specific location
     * @param target coordinates to move toward
     */
    void moveToward(Point2D target);
}
