package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Represents a garden containing flowers and bees. Handles flower and bee interaction.
 */
public class Garden {

    private ArrayList<Bee> bees;
    private ArrayList<Flower> flowers;

    private double width;
    private double height;

    private int spriteSize;
    private int stepCount;
    private boolean done = false;

    /**
     * Create a new garden
     * @param width width of garden in pixels
     * @param height height of garden in pixels
     * @param spriteSize size of sprites in pixels
     * @param numberOfBees number of bees
     * @param numberOfFlowers number of flowers
     */
    public Garden(double width, double height, int spriteSize, int numberOfBees, int numberOfFlowers) {

        this.width = width;
        this.height = height;
        this.spriteSize = spriteSize;

        this.bees = new ArrayList<>();
        this.flowers = new ArrayList<>();

        for (int i = 0; i < numberOfBees; i++) {
            bees.add((Math.random() > .5)
                    ? new DecoratorManager(new BeeRandom(randomPoint()))
                    : new DecoratorManager(new BeeBiased(randomPoint())));
        }
        for (int i = 0; i < numberOfFlowers; i++) {
            double r = Math.random();
            if (r < 0.1) {
                flowers.add(new FlowerDrainEnergy(randomPoint(), new Shield()));
            } else if (r < 0.2) {
                flowers.add(new FlowerDrainEnergy(randomPoint(), new SpeedBoost()));
            } else if (r < 0.3) {
                flowers.add(new FlowerProvideEnergy(randomPoint(), null));
            } else if (r < 0.5) {
                flowers.add(new FlowerProvideEnergy(randomPoint(), new SpeedBoost()));
            } else {
                flowers.add(new FlowerProvideEnergy(randomPoint(), new Hypnosis()));
            }
        }
    }

    /**
     * Retreive the list of flowers from the garden
     * @return flowers
     */
    public ArrayList<Flower> getFlowers() {
        return flowers;
    }

    /**
     * Get the width of the garden
     * @return width in pixels
     */
    public double getWidth() {
        return width;
    }

    /**
     * Get the height of the garden
     * @return height in pixels
     */
    public double getHeight() {
        return height;
    }

    /**
     * Get sprite size
     * @return size in pixels
     */
    public int getSpriteSize() {
        return spriteSize;
    }

    /**
     * Get the number of ticks that have elapsed since the simulation began
     * @return number of ticks
     */
    public int getStepCount() {
        return stepCount;
    }

    /**
     * Is the simulation complete, aka all bees are dead?
     * @return whether or not it is complete
     */
    public boolean isDone() {
        return done;
    }

    /**
     * Move the simulation forward one tick
     */
    public void step() {
        stepCount++;

        // wrap bees around if they go off screen
        Iterator<Bee> beeIterator = bees.iterator();
        while (beeIterator.hasNext()) {

            Bee bee = beeIterator.next();

            if (flowers.size() > 0) {
                bee.move(this);
            } else {
                bee.move(this);
            }

            if (stepCount % 2 == 0) {
                bee.addEnergy(-1);
            }

            if (bee.isDead()) {
                beeIterator.remove();
                if (bees.size() == 0) {
                }
            }

            // ConcreteBee interaction with flowers
            Iterator<Flower> flowerIterator = flowers.iterator();
            while (flowerIterator.hasNext()) {
                Flower flower = flowerIterator.next();

                if (bee.getLocation().distance(flower.getLocation()) < 20) {
                    bee.addEnergy(flower.transferEnergy(bee.getEnergy()));

                    if (flower.getDecorator() != null) {
                        ((DecoratorManager) bee).apply(flower.getDecorator());
                        flower.setDecorator(null);
                    }

                    if (flower.getNectarEnergy() <= 0) {
                        flowerIterator.remove();
                    }

                    bee.setMovementTimeout(2);
                }
            }

            // Bees attack other bees when they collide
            for (Bee otherBee : bees) {
                if (bee.getLocation().distance(otherBee.getLocation()) < 20
                        && bee.getLocation().distance(otherBee.getLocation()) > 0) { // ensure it's not the same bee
                    bee.addEnergy(-5);
                    bee.setMovementTimeout(2);
                }
            }
        }

        if (bees.size() == 0) {
            done = true;
        }
    }

    /**
     * Draw the garden and its contents to the canvas
     * @param ctx context to access canvas
     */
    public void draw(GraphicsContext ctx) {
        ctx.setFill(Paint.valueOf("green"));
        ctx.fillRect(0, 0, width, height);

        for (Flower flower : flowers) {
            flower.draw(ctx);
        }

        for (Bee bee : bees) {
            bee.draw(ctx);
        }
    }

    /**
     * Returns a random location on the grid
     * @return random point
     */
    private Point2D randomPoint() {
        return new Point2D(
                Math.random() * (width - spriteSize),
                Math.random() * (height - spriteSize));
    }
}
