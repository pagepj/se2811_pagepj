package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * Specific implementation of ConcreteBee. Moves toward a given location
 */
public class BeeBiased extends ConcreteBee {

    /**
     * Default constructor
     * @param location initial location of the bee
     */
    public BeeBiased(Point2D location) {
        super(location);
        setSprite(new Image("file:res/bee2.png"));
    }

    /**
     * Move toward a given point.
     * @param garden the garden the bee lives in
     * @return new location
     */
    @Override
    public Point2D move(Garden garden) {

        Point2D target = new Point2D(0, 0);
        double smallestDistance = Double.MAX_VALUE;

        for (Flower flower : garden.getFlowers()) {

            double distance = this.getLocation().distance(flower.getLocation());
            if (distance < smallestDistance) {
                smallestDistance = distance;
                target = flower.getLocation();
            }
        }

        moveToward(target);

        checkEdges(garden);
        return getLocation();
    }

    /**
     * Get the type of bee
     * @return bee type
     */
    @Override
    public String getType(){
        return "BeeBiased";
    }
}
