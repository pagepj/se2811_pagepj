package gardensimulator;

import javafx.scene.image.Image;

/**
 * Bee decorator that shields the bee from a certain amount of damage.
 */
public class Shield extends BeeDecorator {

    private static final int DEFAULT_SHIELD_HEALTH = 50;

    private int shieldHealth;

    /**
     * Wrap the bee with the new functionality
     * @param bee bee to wrap
     */
    public Shield(Bee bee) {
        super(bee);
        this.shieldHealth = DEFAULT_SHIELD_HEALTH;
        this.decoratorSprite = new Image("file:res/shield.png");
    }

    /**
     * Default constructor to allow templates
     */
    public Shield() {}

    /**
     * Instead of removing energy from the bee, remove it from the shield until the shield runs out.
     * @param amount amount to add
     */
    @Override
    public void addEnergy(int amount) {

        // If we still have a shield left...
        if (active) {
            // if the amount is negative (we're subtracting energy)...
            if (amount < 0) {

                // if we have more shield than they're subtracting...
                if (this.shieldHealth > Math.abs(amount)) {

                    // take the full amount from the shield and none from the bee.
                    this.shieldHealth += amount;
                    amount = 0;

                } else {
                    // otherwise, take some of the damage to the shield and the rest to the bee,
                    // mark the shield as depleted
                    amount += shieldHealth;
                    shieldHealth = 0;
                    active = false;
                }
            }
        }

        // call addEnergy with the new amount
        wrappedBee.addEnergy(amount);
    }
}
