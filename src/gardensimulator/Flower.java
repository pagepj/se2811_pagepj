package gardensimulator;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;

/**
 * Abstract flower representation. Implemented by flowers with specific methods to provide or drain energy from bees.
 */
public abstract class Flower {
    private int nectarEnergy;
    private Point2D location;
    private Image sprite;

    private BeeDecorator decorator;
    /**
     * Default constructor.
     * @param location initial location of the flower
     */
    public Flower(Point2D location, BeeDecorator decorator) {
        this.location = location;
        this.nectarEnergy = 5;
        this.sprite = new Image("file:res/flower.png");
        this.decorator = decorator;
    }

    /**
     * Get the bee decorator attached to the flower
     * @return the decorator
     */
    public BeeDecorator getDecorator() {
        return decorator;
    }

    /**
     * Set the bee decorator attached to the flower
     * @param decorator the decorator
     */
    public void setDecorator(BeeDecorator decorator) {
        this.decorator = decorator;
    }

    /**
     * Get current location of bee
     * @return current location
     */
    public Point2D getLocation(){
        return location;
    }

    /**
     * Add the given amount of nectar to the flower
     * @param amount amount to add
     */
    protected void changeNectarEnergy(int amount) {
        this.nectarEnergy += amount;
    }

    /**
     * Get the amount of nectar energy that the flower has
     * @return nectar energy points
     */
    public int getNectarEnergy(){
        return nectarEnergy;
    }

    /**
     * Transfer energy from the flower to the bee (or vice versa)
     * @param beeEnergy current energy level of bee
     * @return amount to give to the bee (can be negative)
     */
    public abstract int transferEnergy(int beeEnergy);

    /**
     * Set the sprite of the flower to an image
     * @param img the image to set
     */
    public void setSprite(Image img) {
        this.sprite = img;
    }

    /**
     * draw the flower to the canvas
     * @param ctx context to access the canvas
     */
    public void draw(GraphicsContext ctx) {
        ctx.drawImage(
                this.sprite,
                this.getLocation().getX(), this.getLocation().getY(),
                50, 50);
        ctx.setFill(Paint.valueOf("White"));
        ctx.fillText(String.valueOf(this.getNectarEnergy()),
                this.getLocation().getX(), this.getLocation().getY());
    }
}
