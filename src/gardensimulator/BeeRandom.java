package gardensimulator;

import javafx.geometry.Point2D;

/**
 * Specific implementation of the bee class. Moves in a random direction.
 */
public class BeeRandom extends ConcreteBee {

    /**
     * Default Constructor
     * @param location location to move toward
     */
    public BeeRandom(Point2D location) {
        super(location);
    }

    private int count = 0; // count the number of frames
    private double angle = Math.random() * 2 * Math.PI;

    /**
     * Move in a random direction
     * @param garden the garden the bee lives in
     * @return the new location
     */
    @Override
    public Point2D move(Garden garden) {
        count++;

        if (count >= 100) {
            angle = Math.random() * 2 * Math.PI;
            count = 0;
        }

        double dx = getSpeed() * Math.cos(angle);
        double dy = getSpeed() * Math.sin(angle);

        setLocation(getLocation().add(dx, dy));
        checkEdges(garden);

        return getLocation();
    }

    /**
     * Get the type of bee
     * @return bee type
     */
    @Override
    public String getType(){
        return "BeeRandom";
    }
}
