package gardensimulator;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import java.io.File;

/**
 * Main entry point for the application
 */
public class Main extends Application {

    /**
     * Starts the application
     * @param primaryStage the main window
     * @throws Exception ignored
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        File file = new File("res/Fruity.wav");
        Media media = new Media(file.toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        //mediaPlayer.setAutoPlay(true);
        MediaView mediaView = new MediaView(mediaPlayer);

        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        primaryStage.setTitle("Garden Simulator");
        Scene scene = new Scene(root, 850, 675);

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    /**
     * Runs the program with the arguments provided
     * @param args ignored
     */
    public static void main(String[] args) {
        launch(args);
    }
}
