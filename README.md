# README #

In the Flowers and Bees Lab, an attempt was made to figure different relationships between classes in a real-life scenario (developing a game).
In this, we were able to see how design patterns are developed, and why they are preferred methods of design leading to implementation. Because
the game can have some nuanced and complex relationships, the lab doubles as an introduction to how coupling and cohesion come into play when
working on code and building upon it, as well as handling technical debt when it comes to an in-depth two-week project. It also served as a more
advanced take on JavaFX.

### Where to find things? ###

* Domain-level EA diagram: doc/ea_domain.eap
* Implementation-level EA diagram: doc/ea_implementation.eap
* Detailed list of requirements: doc/bee_requirements.md

### What we learned ###

What was learned from this lab was that, first of all, it is very important to get a sufficient start on a lab, even if there
is about a month given to complete it. Next, is that it is okay to change the design pattern late in the project, and perhaps
even better than building on technical debt. Also, it is highly important to frequently check that your implementation still
follows the guidelines described in the initial lab list of requirements. Just because we thought something was more fun or
better, doesn't necessarily mean that was being asked for. Lastly, it is very important to push and pull, to and from the
repository on a very frequent basis.

### Things we liked or suggestions for improvement ###
This was a very fun and straightforward lab to complete. A couple facets of it were more complicated than the rest, which
we would have appreciated a little more guidance with. Understandably a large portion of becoming a working professional in the
professional world involves learning things from your own means, but a couple of things would have been nice to learn in
class, even in like SE1021. For instance, putting pictures in JavaFX on the canvas, or learning how to use multi-threading.